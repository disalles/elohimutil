package com.elohim.infrastructure.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="Cliente")
@SequenceGenerator(name="cliente_seq" ,sequenceName="cliente_id_seq",allocationSize=1)
public class Cliente {

	@Id
	@GeneratedValue(generator="cliente_seq",strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(name="nome")
	private String name;
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Cliente()
	{
		
	}
}
