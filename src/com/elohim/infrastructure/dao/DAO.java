package com.elohim.infrastructure.dao;

import java.util.List;

import javax.persistence.Query;


public interface DAO <T> {
	
	DAO<T> getInstance (Class<T> classe);
	boolean add (T elemento);
	void  addAll (List <T> t);
	T update(T t);
    T find(long id);
    T get (T t);
    Query Query(String query);
    List<Object> getAll();
    void remove (T t );
    void removeAll(List<T> list);
    void truncate (T t);
    void setParameterClass(Class<T> classe);
    void setPersistenceManager(Object object);
}
