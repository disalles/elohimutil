package com.elohim.infrastructure.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.springframework.stereotype.Repository;




@Repository
public  class GenericJPADAO<T> implements DAO<T> {
	
	private Logger logger;
	@PersistenceContext(name="casaconforto")
	private EntityManager entityManager;
	private Class<T> classe;


	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public GenericJPADAO() {
	}



	public GenericJPADAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}


	@Override
	public boolean add(T t) {
	try {
			entityManager.persist(t);
		} catch (Exception ex) {
		    logger.error(ex.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	public void addAll(List<T> list) {
		for (T t : list)
			entityManager.merge(t);

	}

	@Override
	public T get(T t) {
		T entity = entityManager.merge(t);
		return entity;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getAll() {

		Query query = entityManager.createQuery("SELECT e from Object");

		return query.getResultList();
	}

	@Override
	public void remove(T t) {
		entityManager.remove(t);
	}

	@Override
	public void removeAll(List<T> list) {
		for (T t : list)
			entityManager.refresh(t);
	}

	@Override
	public void truncate(T t) {
		Query query = entityManager
				.createNamedQuery("TRUNCATE TABLE :tableName");
		query.setParameter("tableName", t);
		query.executeUpdate();
	}
	@Override
	public void setPersistenceManager(Object object) {
		this.entityManager = (EntityManager) object;
	}

	@Override
	public T update(T t) {
		return entityManager.merge(t);
	}

	@Override
	public DAO<T> getInstance(Class<T> classe) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T find(long id) {
		return entityManager.find(classe, id);
	}

	@Override
	public void setParameterClass(Class<T> classe) {
		this.classe = classe;
	}

	@Override
	public javax.persistence.Query Query(String query) {
		return entityManager.createQuery(query);
	}


}
