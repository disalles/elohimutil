package com.elohim.infrastructure.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;


public class SpringJPADAO<T> implements DAO<T> {

	public Logger logger = Logger.getLogger(SpringJPADAO.class);
	
	@PersistenceContext(name="casaconforto")
	private EntityManager entityManager;

	private Class<T> classe;

	public  SpringJPADAO() {

	}

	public  SpringJPADAO(Class<T> classe) {
		this.classe = classe;

	}

	public  SpringJPADAO(Class<T> classe, EntityManager entityManager) {
		this.entityManager = entityManager;
		this.classe = classe;
	}

	public void setParameterClass(Class<T> classe) {
		this.classe = classe;
	}

	@Override
	@Transactional
	public boolean add(T t) {
		try {
			entityManager.persist(t);
			
		} catch (Exception ex) {
		   // logger.error(ex.getMessage());
			return false;
		}
		
		return true;
	}

	@Override
	@Transactional
	public void addAll(List<T> list) {
		for (T t : list)
			entityManager.persist(t);
	}

	@Override
	@Transactional
	public T get(T t) {
		T entity = entityManager.merge(t);
		
		return entity;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> getAll() {

		Query query = entityManager.createQuery("SELECT e from Object");

		return query.getResultList();
	}

	@Override
	@Transactional
	public void remove(T t) {
		entityManager.remove(t);
	}

	@Override
	@Transactional
	public void removeAll(List<T> list) {
		for (T t : list)
			entityManager.remove(t);
	}

	@Override
	@Transactional
	public void truncate(T t) {
		Query query = entityManager
				.createNamedQuery("TRUNCATE TABLE :tableName");
		query.setParameter("tableName", t);
		query.executeUpdate();
	}

	@Override
	public T find(long id) {
		return entityManager.find(classe, id);
	}
	
	@PersistenceContext
	public void setPersistenceManager(Object entityManager) {
		this.entityManager = (EntityManager) entityManager;
	}

	@Override
	@Transactional
	public T update(T t) {
		T entidade = entityManager.merge(t);
		return entidade;
	}

	@Override
	public javax.persistence.Query Query(String query) {
		return entityManager.createQuery(query);
	}

	@Override
	public DAO<T> getInstance(Class<T> classe) {
		// TODO Auto-generated method stub
		return null;
	}
}
