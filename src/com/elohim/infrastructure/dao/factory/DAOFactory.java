package com.elohim.infrastructure.dao.factory;

import com.elohim.infrastructure.dao.DAO;


/**
 * Classe responsavel por fabricar {@link DAO} pela classe da entidade
 * 
 * @author Bruno Alcantara
 */
public final class DAOFactory {

	private DAOFactory() {
		super();
	}

	/**
	 * Cria um {@link DAO} para a classe da entidade passada
	 * 
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	public static <T> DAO<T> create(Class<T> classe, String nomePersiste)
			throws IllegalAccessException {
		Class<T> obj;

		DAO<T> dao = null;

		try {
			obj = (Class<T>) Class.forName(nomePersiste);
			System.out.println(obj.getName());
			dao = (DAO<T>) obj.newInstance();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		dao.setParameterClass(classe);
		
		return dao;
	}
}
