package com.elohim.infrastructure.dao.factory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.Test;

import com.elohim.infrastructure.dao.Cliente;
import com.elohim.infrastructure.dao.DAO;
import com.elohim.infrastructure.dao.factory.DAOFactory;
import com.elohim.infrastructure.util.PropertiesLoader;

public class DAOTest {
	

	private EntityManagerFactory emf = null;
    private EntityManager em = null;
	
	@Before
	public void initTest ()
	{
	            if (emf == null)
	                 emf = Persistence.createEntityManagerFactory("casaconforto");
	            
	          em = emf.createEntityManager();	
	}
	
	@Test
	public void addDAOTest() throws IllegalAccessException
	{
	
		Cliente cliente = new Cliente();
		cliente.setName("Diego");
		
		
		DAO<Cliente> dao = DAOFactory.create(Cliente.class,new PropertiesLoader().getValor("speaker.persist"));
	    dao.setPersistenceManager(em);
	    
		dao.add(cliente);
		
		
	}

}
